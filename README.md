Disordered
==========

## Development
`npm run dev`

### global deps (if need, usually for dev on windows)
`npm i -g webpack webpack-dev-server`

## Production
`npm run build` - Build with Webpack
